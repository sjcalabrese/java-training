package com.revature.junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AllTests.class, ArithmaticTest.class, ArithmaticTest2.class, GenaricTest.class })
public class AllTests2 {

}
