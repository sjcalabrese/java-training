package com.revature.junit;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.revature.arithmatic.Arithmatic;

public class ArithmaticTest {
	Arithmatic a = null;
	
	@Before
	public void setUp() {
		a = new Arithmatic();
	}
	
	@After
	public void tearDown() {
		a = null;
	}
	@Test
	public void testAddition() {
		assertEquals(30, a.addition(10,5,14,1));
	}
	@Test
	public void testSubtraction() {
		assertEquals(5, a.subtraction(10,5));
		assertEquals(0, a.subtraction(0));
	}
	@Test
	public void testMultiplication() {
		assertEquals(0, a.multiplication());
		assertEquals(8, a.multiplication(2,4));		
	}
	@Test
	public void testDivision() {
		assertEquals(0, a.division());
		assertEquals(4, a.division(8,2));
//		assertEquals(0, a.division(1,0));
	}
	@Test(expected=ArithmeticException.class)
	public void testDivByZero() {
		a.division(1,0);
	}
	@Ignore
	@Test//(expected=NullPointerException.class)
	public void testDivNull() {
		a.division(null);
	}
//	@Test(timeout=3000)
//	public void timeWaster() {
//		while(true) {}
//	}
}
