package com.revature.pillars;
/*
 * BEANS BEANS BEANS!
 */
public class Arm {
	private int length;

	public Arm() {
		super();
	}

	public Arm(int length) {
		super();
		this.length = length;
	}


	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}
}
