package com.revature.pillars;

public class Gorilla extends Animal implements AnimalActions{
	/*
	 * The fact that the Gorilla extends animal mean that 
	 * one can say: "a Gorilla IS an animal"
	 * 
	 * The fact that the Gorilla with the object
	 * ARM, means that one can say: "A Gorilla HAS an arm"
	 */
	
	private int teeth;
	private Arm leftArm;
	private Arm rightArm;
	
	@Override
	public int getTeeth() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String eat() {
		return "Gorilla is having lunch";
	}
	/*
	 * Below we have Override preceded by an "at" symbol, this is called an annotation
	 * It provides metadata and additional checks for Java applications
	 */

	@Override
	public String move() {
		// TODO Auto-generated method stub
		return null;
	}

	//Overloading
	public Gorilla(int teeth, Arm leftArm, Arm rightArm) {
		super();
		this.teeth = teeth;
		this.leftArm = leftArm;
		this.rightArm = rightArm;
	}

	public Gorilla() {
		super();
	}

	public Arm getLeftArm() {
		return leftArm;
	}

	public void setLeftArm(Arm leftArm) {
		this.leftArm = leftArm;
	}

	public Arm getRightArm() {
		return rightArm;
	}

	public void setRightArm(Arm rightArm) {
		this.rightArm = rightArm;
	}

	public void setTeeth(int teeth) {
		this.teeth = teeth;
	}

	@Override
	public String toString() {
		return "Gorilla [teeth=" + teeth + ", leftArm=" + leftArm + ", rightArm=" + rightArm + "]";
	}
	
	 
}
