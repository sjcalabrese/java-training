package com.revature.pillars;

public class Driver {

	public static void main(String[] args) {
		covariance();
	}

	public static void covariance() {
		A a = new A();
		B b = new B();
		C c = new C();
		System.out.println("======START==========");
		System.out.println(a.field);
		System.out.println(b.field);
		System.out.println(c.field);
		a.method1();
		b.method1();
		c.method1();
		A a2 = new C();
		//remember that you get the reference's version of the filed
		System.out.println(a2.field);
		//get the most specific instances method.
		a2.method1();
		System.out.println("+++++++++++++++");
		a = b;
		a.method1();
		System.out.println(a.field);
		
		System.out.println("========END==========");
	}
}
