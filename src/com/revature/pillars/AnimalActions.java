package com.revature.pillars;

public interface AnimalActions {
	int i = 10;

	public String eat();

	public String move();

	public static void method() {
		System.out.println("Static implementation");
	}

	public default void defaultMethod() {
		System.out.println("Default implementation");
	}
}
