package com.revature.controlflow;

import java.util.Scanner;

public class Control {
	public static void main(String[] args) {
		//control1();
		//enhanced();
		//ternary();
		//shortCircuit();
		//breakControl();
		switchStatement();
	}
	
	public static void control1() {
		Scanner scan = new Scanner(System.in);
		System.out.println("Input an int value: ");
		int x = scan.nextInt();
		
//		if statements
//		if(x > 3) {
//			System.out.println("x is greater than 3");
//		}
//		if(x == 3) {
//			System.out.println("x is equal to 3");
//		}else {
//			System.out.println("x is obviously not 3...");
//		}
//		
//		for(int i = 0;i<x;i++) {
//			System.out.println(i);
//		}
		//while loops
//		int i = 0;
//		while(i < x) {
//			System.out.println(i);
//			i++;
//		}
		//do-while loop
//		int i = 0;
//		do {
//			System.out.println(i);
//			i++;
//		}while(i<x);
		}//method1 method
	
	public static void enhanced() {
		String[] arr = {"String1", "String2","String3"};
		
		for(String str: arr) {
			System.out.println(str);
		}
		System.out.println();
		//traditional for loop doing the same thing
		for(int i = 0; i<arr.length;i++) {
			System.out.println(arr[i]);
		}
	}//enhanced method
	public static void ternary() {
		Scanner scan = new Scanner(System.in);
		System.out.println("Input a int value: ");
		int x = scan.nextInt();
		
		String s = x>3 ?"x is greater than 3":"x is not greater than 3";
		System.out.println(s);
	}//Ternary method
	
	public static void shortCircuit() {
		int x = 6;
		int y = 6;
		int z = 7;
		System.out.println("x= " + x + ",y= " + y + ",z= " + z);
		if(x==y | y ==z) {
			System.out.println("or true");
		}
		if(x==y & y ==z) {
			System.out.println("and true");
		}
		//using || and && can cause it to be short circuited
		if(x ==y || x== y++) { //does not run the increment statement because of the short circuit
			System.out.println("x= " + x + ",y= " + y + ",z= " + z);
		}
			
	}//shortCircuit method
	public static void breakControl() {
//		for(int i = 0;i<10;i++) {
//			System.out.println(i);
//			if(i==5) {
//				continue;
//			}
//			System.out.println(i);
//			if(i==7) {
//				break;
//			}
		//}
		loop1: for(int i =0;i<10;i++) {
			loop2: for(int j = 0;j<5;j++) {
				if(i==7 && j > 3) {
					break loop1;
				}
				System.out.println(i + " " + j);
			}
		}
	}//break control
	public static void switchStatement() {
		Scanner scan = new Scanner(System.in);
		System.out.println("Gimme a number: ");
		int x = scan.nextInt();
		
		switch(x) {
		
		case 0: System.out.println("x is 0"); break;
		case 1: System.out.println("x is 1"); break;
		case 2: System.out.println("x is 2"); break;
		default: System.out.println("default");
		
		}
	}
}

