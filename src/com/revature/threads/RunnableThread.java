package com.revature.threads;

public class RunnableThread implements Runnable {

	@Override
	public void run() {
		for(int i = 0;i <10; i++) {
			System.out.println("\t" +Thread.currentThread().getName());//gets the name of the thread
			try {
				Thread.sleep(50);;//pause for 50 miliseconds
			}catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
		throw new RuntimeException();
	}

}
