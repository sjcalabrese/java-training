package com.revature.threads;

public class ThreadThread extends Thread {
	/*
	 * In either case, for creating a thread, you much implement the run() method
	 * in order to designate wha twill be executed when you branch the thread off
	 * The thread will fork and execute everything you wrote in the run method. 
	 * In order to actually branch the thread off, you need to invoke the start() method
	 * Should you try to begin a thread by invoking run(), it will simply exectue run()'s 
	 * contents in a sequential manner in the main thread
	 */
	public void run() {
		for(int i = 0;i <50; i++) {
			System.out.println(Thread.currentThread().getName());//gets the name of the thread
			try {
				Thread.sleep(50);;//pause for 50 miliseconds
			}catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
