package com.revature.threads;

public class Driver {

	public static void main(String[] args) {
		System.out.println("============BEGINING OF THREAD EXAMPLE===============");
		Thread t1 = new Thread(new ThreadThread(), "Thread1");
		Thread t2 = new Thread(new RunnableThread(), "Thread2");
		
		t1.start();
		t2.start();
		//The threads are "runnable"
		
		for(int i = 0;i <50; i++) {
			System.out.println("\t\t" +Thread.currentThread().getName());//gets the name of the thread
			
			try {
				Thread.sleep(50);;//pause for 50 miliseconds
			}catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("=========================END===============");		
		//right after here is where the main method terminates.
	}

}
