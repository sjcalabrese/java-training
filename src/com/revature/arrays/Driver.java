package com.revature.arrays;

public class Driver {

	/*
	 * 
	 */
	public static void main(String[] args) {
		int arr[];
		int[] arr2;
		
		arr = new int[5];//capacity of 5
		arr2 = new int[] {1,2,3,4,5};//also valid
		
		//note arrays begin counting at 0
		arr[0]=7;
		//System.out.println(arr[0]);
		for(int x: arr) {
			System.out.println(x);
		}
		
		String[] things = new String[3];
		things[0] = "um";
		things[1] = "Juggggggernaught";
		things[2] = "post-zero";
		
		for(String str : things) {
			System.out.println(str);
		}
		
		//multidimensional
		int[][] twoD = new int[5][5];
		int[][] twoD2 = {{1,2,3},{1,2,4},{1,2,5}};
//		for(int i = 0; i <3;i++) {
//			for(int j = 0;j<3;j++) {
//				System.out.println(twoD2[i][j]);
//			}
	//}
			//enhanced for multidimensional
			for(int[] intArray: twoD2) {
				for(int y: intArray) {
					System.out.println(y);
				}
			}
		}
}
