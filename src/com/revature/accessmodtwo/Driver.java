package com.revature.accessmodtwo;
import com.revature.accessmodone.Modifiers;

public class Driver{

	public static void main(String[] args) {
		
		Modifiers mod = new Modifiers();
		
		System.out.println("=========ACCESSING FIELDS");
		System.out.println(mod.pub);
		//System.out.println(mod.prot); Not in same package or inherited.
		//System.out.println(mod.def); Not same package or class
		///System.out.println(mod.priv); not package, subclass, or global
		System.out.println("========method==========");
		mod.output();
		
		//Driver driver = new Driver();//extends Modifiers variables.
		//System.out.println(driver.pub);
		//System.out.println(driver.prot);
	}

}
