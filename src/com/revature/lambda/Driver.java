package com.revature.lambda;

public class Driver {

	public static void main(String[] args) {
		/*
		 * Lambda expressions are a short hand for creating anonymous methods
		 * They were added in Java 8
		 * following benefits:
		 * -you now have the ability to treat functionality as a method argument
		 * -A function can be created without belonging to any class
		 * -A lambda expression can be passed around as if it was an object
		 */
		FunkyInterface add5 = (num)-> num +=5;
		
		System.out.println(add5.doMath(20));
		
		//not that you can create several instances
		FunkyInterface multiply5 = (num) -> num *=5;
		System.out.println(multiply5.doMath(15));
	}

}
