package com.revature.primitives;

public class PrimitivesAndStrings {
//java has 8 primitive (Non-object) dataTypes
	int i = 32;
	short s;
	long l;
	char c = 'c';
	byte b;
	float f;
	double d;
	boolean bool;
	
	/*
	 * The "default" datatypes for whole number is int
	 * The "default" dataypes for decimals is double
	 */
	
	/*
	 * Another common datatype that you will see is string
	 * Note that strings are not a primitive in java
	 * They ar objects(There is a String class
	 */
	String str;
	
	public static void main(String[] args) {
		PrimitivesAndStrings p = new PrimitivesAndStrings();
		
		p.doStuff();
		p.doCast();
		
	}
	
	public void doStuff() {
		System.out.println(i);
		System.out.println(s);
		System.out.println(l);
		System.out.println(b);
		System.out.println(c);
		System.out.println(f);
		System.out.println(bool);
		
	}
	public void doCast() {
		l = i; //allowed because small int is going into long
		i = (int)l; //forces the compiler to put a larger datatype into a smaller one
		//i = f; not good because you will loose info
		i = (int)f; //makes it happen but will truncate decimal
		i = c; //gets the numerical equation for the value
		b = (byte)c; //cast a byte to a char
	}

}
