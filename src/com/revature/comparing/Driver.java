package com.revature.comparing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Driver {

	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		
		list.add("stuff");
		list.add("more..stuff");
		list.add("and things");
		System.out.println(list);
		list.sort(null);//sort according to natural order based on strings natural order
		System.out.println(list);//now in order (yay!!!)
		
		List<Person> people = Arrays.asList(
				new Person("John Smith", 27),
				new Person("Juan Sanchez", 99),
				new Person("My name is....Jeff", 33),
				new Person("Mario", 50)
				);//immutable //creates and populates list 
				//note, this list is immmutable this way
		for(Person p: people) {
			System.out.println(p);
		}
		//people.add(new Person("me", 1000));//gives an exception because it's too large
		people.sort(null);//uses object's compararTo() method created in Person class
		System.out.println("====AFTER SORTING======");
		for(Person p: people) {
			System.out.println(p);
		}
		
		System.out.println("========comparators=============");
		people.sort(new PersonNameComparator());
		for(Person p: people) {
			System.out.println(p);
		}
	}

}
