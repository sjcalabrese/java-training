package com.revature.scanner;

import java.util.Scanner;

public class Driver {

	
	public static void main(String[] args) {
		System.out.println("=========START===========");
		
		Scanner scanner = new Scanner(System.in);
		String input = "";
		System.out.println("Tell me a thing...and I will repeat it to you!");
		
		//input = scanner.next();this line is what takes user input until it reaches a space
		input = scanner.nextLine();//reads until it reaches a new line character
		System.out.println("Your input was " + "'" + input + "'");
		System.out.println("=================END================");

	}

}
