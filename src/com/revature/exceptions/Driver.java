package com.revature.exceptions;

import java.io.FileNotFoundException;

public class Driver {

	public static void main(String[] args) throws FileNotFoundException {
		Driver driver = new Driver();
		System.out.println("=========START===========");
		// driver.divideByZero();
		// driver.divideByZeroHandled();
		//
//		driver.checkedExceptions();// throws from here, because there is nowhere else to pass the throw to
		System.out.println(driver.somethingWeird());
		System.out.println("==========END============");

	}

	public void divideByZero() {
		System.out.println((1 / 0));
	}

	public void divideByZeroHandled() {
		try {
			// write code here that may be suspect to cause an exceptino
			// System.out.println(1/0);
			throw new ArithmeticException();
		} catch (ArithmeticException e) {
			// place code in here to describe executino in case of an exception
			// This code will execute instead of the system crashing due to the exeption

			e.printStackTrace();// prints the stack trace for the exception
			System.out.println("Got EEEM");
		} finally {
			System.out.println("I always run! Yay");
		}
	}

	public void checkedExceptions() throws FileNotFoundException {
		/*
		 * you can use thrown to trigger an exception of your choice
		 * 
		 */
		try {
			throw new FileNotFoundException();// without throw in the method it wants you to use try catch.
		} catch (FileNotFoundException e) {

		}
	}

	public void method1() throws WillException {
		method2();
	}

	public void method2() throws WillException {
		method3();
	}

	public void method3() throws WillException {
		throw new WillException();
	}
	
	public String somethingWeird() {
		try {
			System.out.println(1/0);
			return "TRY";
		}catch(Exception e) {//generally not good practice because it is too broad
			return "catch";
		}finally {
			return "finally";
		}
	}
	
	public void goodHandline() {
		try {
			
		}catch(ArithmeticException e) {//sibling so order doesn't matter. but child of RuntimeExceptions
			e.printStackTrace();
		}catch(NullPointerException e) {//sibling
			e.printStackTrace();
		}
		catch(RuntimeException e) {
			e.printStackTrace();
		}catch(Exception e) {
			e.printStackTrace();
		}catch(Throwable e) {
			//this catch catches everything that descends from thowable/since exception is inherited.
		}
		
	}
}
