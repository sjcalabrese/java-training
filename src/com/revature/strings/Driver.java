package com.revature.strings;

public class Driver {

	public static void main(String[] args) {
		String s1 = "beisbol";
		System.out.println("s1: " + System.identityHashCode(s1));
		s1 = s1 + "ball";
		System.out.println("s1: " + System.identityHashCode(s1));
		System.out.println(s1);
		System.out.println("====================");
		
		String s2 = "socker";
		String s3 = "socker";
		System.out.println("s2: " + System.identityHashCode(s2));
		System.out.println("s3: " + System.identityHashCode(s3));
		
		System.out.println(s2==s3);//true because location in memory is the same(string pool)
		s2 += "s";
		s3 += "s";
		System.out.println("s2: " + System.identityHashCode(s2));
		System.out.println("s3: " + System.identityHashCode(s3));
		System.out.println(s2==s3);//not guarenteed to be in the same place in the string pool after concat operator
		s2=s2.intern();//points to, or creates then point to
		s3=s3.intern();//corrects the location pointer in the string pool after concat.
		System.out.println("s2: " + System.identityHashCode(s2));
		System.out.println("s3: " + System.identityHashCode(s3));
		System.out.println(s2==s3);
		
		System.out.println("=============BUILDER/BUFFER=======================");
		StringBuilder sbui = new StringBuilder("Stuff");
		System.out.println(sbui);
		System.out.println(System.identityHashCode(sbui));
		sbui.append(" and things");
		System.out.println(sbui);
		System.out.println(System.identityHashCode(sbui));		
		
		
	}

}
