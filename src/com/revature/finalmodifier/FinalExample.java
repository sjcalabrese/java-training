package com.revature.finalmodifier;

//cannot inherit from final class
public class FinalExample {
	
	final int FINAL_FIELD = 12;
	
	public void method1() {
		
	}//cannot be overridden, but because the class is final
	
	public final void method2() {
		
	}//cannot be overridden because it's final
}
	/*
	 * 
	 */

