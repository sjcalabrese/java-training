package com.revature.finalmodifier;

// Note that final classes cannot be inherited.
public final class FinalClass {
	
	final int FINAL_FIELD = 12;
	
	public void method1() {
		
	}//cannot be overridden, but because the class is final
	
	public final void method2() {
		
	}//cannot be overridden because it's final
}
