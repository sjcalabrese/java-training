package com.revature.finalmodifier;

public class Driver extends FinalExample{

	public static void main(String[] args) {
		FinalExample fe = new FinalExample();
		System.out.println(fe.FINAL_FIELD);
		
		//fe.FINAL_FIELD = 99; Cannot be changed because it is final
	}
	public void method1() {
		super.method1();
	}
	
//	pulblic void method2() {
//		super.method2();
//	} cannot be changed because it is a protected method
}
