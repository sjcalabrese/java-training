//package must be the first line of your Java file (before any imports or classes.)
package com.revature.helloworld;

//imports...
//With imports, you can import other class and other packages to use
//*****implicitly imports java.lang.* ********;

//you can only have one public class in a Java file(can have other classes)
//note the name of the public class must match the java file name
public class HelloWorld {
//to run or execute a file, we need a main method
//this main method must follow the following method signature
	public static void main(String[] args) {
		System.out.println("Hello World!");
	}
/*Multi 
 * Line 
 * comment	
 */
/**Java doc comment
 * These  will show
 */
	
}
