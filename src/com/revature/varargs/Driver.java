package com.revature.varargs;

public class Driver {

	
	public static void main(String[] args) {
		
		Driver driver = new Driver();
		
		System.out.println(driver.sum(1, 2,3,5,6,7));
	}

	public int sum(int a, int b) {
		return a + b;
	}
	
	public int sum(int a, int b, int c) {
		return a + b + c;
	}
	
	public int sum(int a, int ... nums) {//receives any number of int arguments
		//then puts them in the array nums.
		int result=a;
		for(int i: nums) {
			result += i;
		}
		return result;
	}
}
