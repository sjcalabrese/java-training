package com.revature.designpatterns;

public class SingletonObject {
	private static SingletonObject s = null;
	
	private SingletonObject() {
		
	}
	
	public static SingletonObject getSingleton() {
		if(s==null) {
			s= new SingletonObject();
		}
		return s;
	}
}
