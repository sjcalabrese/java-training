package com.revature.designpatterns;

public class ShapeFactory {
	public Shape getShape(String request) {
		switch(request.toLowerCase()) {
		case "circle":
			return new Circle();
		case "square":
			return new Square();
		case "triangle":
			return new Triangle();
		default:
		return null;
	}}
}
