package com.revature.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;

import java.util.ListIterator;

public class Driver {

	public static void main(String[] args) {
		System.out.println("============LISTS==============");
		ArrayList l1 = new ArrayList();

		l1.add(1);
		l1.add("1");
		l1.add(true);
		System.out.println(l1);
		System.out.println(l1.remove(true));// autoboxing into an object

		System.out.println("========GENERIC LIST===========");
		List<Integer> nums = new ArrayList<>();// cannot use int
		nums.add(15);// autoboxing occurs
		nums.add(new Integer(200));// no autoboxing
		nums.add(38);
		nums.add(3278);
		// nums.add("string");cannot be done because of generic enforcement
		System.out.println(nums);

		System.out.println(nums.get(2));// Retrieves an item at a specific index doesn't use [] because
		// it's not an array.
		System.out.println(nums.remove(1));//
		System.out.println(nums.remove(new Integer(39)));
		System.out.println(nums);

		nums.set(1, 25);
		System.out.println(nums);
		System.out.println("=======ITERATING THROUGH COLLECTION========");
		for (Integer i : nums) { // iterates over whole list
			System.out.println(i);
		}

		Iterator it = nums.iterator();
		System.out.println(it.next());
		System.out.println(it.next());
		System.out.println(it.next());

		System.out.println("+++++LIST ITERATOR++++++");
		
		ListIterator li = nums.listIterator();
		while (li.hasNext()) {
			System.out.println(li.next());
		}
		while( li.hasPrevious()) {
			System.out.println(li.previous());
		}

		
		System.out.println("=========SETS=========");
		Set<Integer> set = new HashSet<>();
		set.add(1);
		set.add(2);
		set.add(3);
		set.add(3);
		set.add(467);
		set.add(288);
		System.out.println(set);

		set.remove(467);
		System.out.println(set);
			int count = 0;
			for (Integer i : set) {
			if (count == 3) {
				System.out.println(i);
				break;
			}
			count++;
		}//note that sets do not support random access, if you want a particular item,
		//you would have to loop through the set
		System.out.println(set.contains(3));
		
		System.out.println("=============queue=============");
		Queue<Integer> q = new ArrayBlockingQueue<>(4);//queue that specifies a size
		q.offer(5);//inserts from the back of the queue
		q.offer(10);
		q.offer(15);
		q.offer(1);
		System.out.println(q);
		
		Integer result;
		while((result = q.poll()) !=null) {//poll removes from font
			System.out.println(result);
			System.out.	println(q);
		}
		q.offer(5);//inserts from the back of the queue
		q.offer(8);
		q.offer(9);
		q.offer(10);
		System.out.println(q.peek());
		System.out.println(q);
		
		System.out.println("===========MAPS===============");
		Map<Integer,String> map = new HashMap<>();
		map.put(3, "treasure");
		map.put(6, "boot-e");
		map.put(1, "goggle-maps");
		map.put(3, "Sassy Inteli-sense");//overrides existing value
		map.put(9, null);
		System.out.println(map.size());
		System.out.println(map.entrySet());
		System.out.println(map);
		
		for(Integer i: map.keySet()) {
			System.out.println(map.get(i));//when using the get method you will be returned the value
			//based on the key entered.
		}
		
		//Hash Table
		Map<Integer, String> ht = new Hashtable<Integer, String>();
		ht.put(1, "one");
		ht.put(2, "something");
		ht.put(3, "something else");
		//ht.put(null, null);//exception occurs
		//ht.put(5, null);//exception also occurs
		System.out.println(ht);
	}
	
}
