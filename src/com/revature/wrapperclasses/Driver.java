package com.revature.wrapperclasses;

public class Driver {
/*
 * primitive datatypes (all eight) are NOT objects
 * However, if this lack of Object-ness leaves a hole in your heart, well..... there is some good news
 * 
 * Java provides a Wrapper Classes, which are Object representations of the primitive datatypes
 * 
 * To acess them, simply write the datatype in its full name and preeded with a capital letter
 */
	//int i = 9; //primitive
	Integer i = 9;// now an object type integer
	Integer i2 = new Integer(9);
	Double d;
	Character c;
	Boolean bool;

	public static void main(String[] args) {
		Driver driver = new Driver();
		driver.printValues();
		System.out.println(driver.addition(5, 6));
	}
	public void printValues() {
		System.out.println(i);
		i=7; //This is an example of autoboxing
		System.out.println(i);
		
		Integer i3 = Integer.MAX_VALUE;
		System.out.println(i3);
	}
	public Integer addition(Integer a, int b) {//return type it Integer(so autoboxes)
		//return new Integer(a.intValue() + b);
		return a.intValue() + b;//same //makes a an int, then add a and b, then autoboxes
		//into an Integer object.
	}
	public int addition2() {
		return new Integer(5);//unboxes to int type for return type.
	}
	
}
