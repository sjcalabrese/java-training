package com.revature.scopes;

/*
 * Design pattern
 * 	is a tried and true approach/methodology for achieving a design functionallity or structure
 * in programming
 */
public class Scopes {
	public static int count = 0;
	/*
	 * Static Scope;
	 */
	private String brand;
	private String type;

	/*
	 * Instance Scope
	 */
	public Scopes(String brand, String type) {
		this.brand = brand;
		this.type = type;
		count++;
	}

	public Scopes() {
		brand = "Unknown";
		type = "Unknown";
		count++;
	}

	public void randomMethod(int input) {
		int i = 9;
		/*
		 * method Scope
		 */

		for (int j = 0; j < 0; j++) {
			/*
			 * Loop Scope
			 */
		}
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Scopes [brand=" + brand + ", type=" + type + "]";
	}

	
}
