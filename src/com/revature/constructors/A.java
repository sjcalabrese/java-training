package com.revature.constructors;

public class A {

	private int myNumber;

	public A() {
		// super(); the call to super is implicit
		// this(); the alternative to super()
		System.out.println("Printing from A's no arg constructor");
	}// constructor

	public A(int i) {
		System.out.println("Inside 1 arg constructor for A");
		this.myNumber  = i;
	}//single argument constructor
	
	public static void myHiddenMethod() {
		System.out.println("This is Class A's Hidden method");
	}
	

}
