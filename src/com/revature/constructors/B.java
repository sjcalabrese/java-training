package com.revature.constructors;

public class B extends A {

	private int myNumber;

	public B() {
		System.out.println("printing from inside B's no-arg constructor");
	}

	public B(int myNumber) {
		super(myNumber);// calls A's one arg constructor
		System.out.println("Inside the 1 arg constructor of B");
		this.myNumber = myNumber;
	}

	public static void myHiddenMethod() {
		System.out.println("This is Class B's Hidden method");
	}
	
	
}
