package com.revature.accessmodone;

public class Modifiers {

	public String pub = "Public";
	protected String prot = "Protected";
	String def = "Default"; //Default
	private String priv = "Private";
	
	public void output() {
		System.out.println(priv);
		System.out.println(prot);
		System.out.println(def);
		System.out.println(pub);
	}
}
