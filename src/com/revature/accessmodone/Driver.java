package com.revature.accessmodone;

public class Driver {
	

	public static void main(String[] args) {
		Modifiers mod = new Modifiers();
		//Datetype variablename = new className();
		//Reference = Assignment
		
		System.out.println("============ACCESSING FIELDS DIRECTLY==================");
		System.out.println(mod.pub);//public
		System.out.println(mod.prot);//same package, so ok
		System.out.println(mod.def); //same package, so ok
		//System.out.println(mod.priv); compiler doesn't allow it to run.

		System.out.println("========EXECUTING OUTPUT() FROM MODIFIERS===========");
		mod.output();
	}

}
