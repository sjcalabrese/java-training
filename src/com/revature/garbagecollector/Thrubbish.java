package com.revature.garbagecollector;

/*
 * Java does not let you, the developer handle memory, 
 * Java takes full control of that itself, this is done
 * using the Garbage Collector. An executioner who serves to only ensure that we don't have any 
 * unreachable resources that remain. IE, unreferenced
 * 
 * Objects sitting around cluttering up memory
 * Any object in Java is eligible for GC if one of the following criteria is met
 *  -reference is reassigned
 *  -reference is point to null
 *  -Scope of the instance is expired.
 */
public class Thrubbish {

	public int id;

	public Thrubbish(int id) {
		super();
		this.id = id;
		System.out.println("Trash Created, ID:" + id);
	}
		@Override
			protected void finalize() throws Throwable {
				System.out.println("\t\tTrash ID: " + id
						+ "Collect STARTED");
				/*
				 * this method does nothing for GC (functionally)
				 * it is simply the method that is called by the GC prior to actually
				 * reclaiming the memory of the particular object
				 */
			}
}
