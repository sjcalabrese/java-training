package com.revature.garbagecollector;

public class Driver {
	public static void main(String[] args) {
		//this demonstration will show why it is not always most efficient to collect
		//garbage constantly
		for(int i = 0; i < 10000; i++) {
			Thrubbish garbodor = new Thrubbish(i);
			System.gc();//this method suggest to the gc that it ought to take out the trash sooner.
		}
}
}
