package com.revatore.fileio;

import java.io.*;

import jdk.jfr.events.FileWriteEvent;

public class Driver {
	private final String FILE_NAME = "files/myfile.csv";
	private FileOutputStream fos = null;
	private FileInputStream fis = null;
	private FileWriter fw = null;
	private FileReader fr = null;
	private BufferedReader br = null;
	private BufferedWriter bw = null;

	public static void main(String[] args) {
		System.out.println("=====START======");
		Driver driver = new Driver();

		try {
			driver.fosExample();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			driver.fisExample();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			driver.fileWriterAndReaderExample();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			driver.bufferedExample();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("=======END===========");
	}

	public void fosExample() throws IOException {
		/*
		 * file input/outputstreams are able to write/read data ONE BYTE at a time
		 */
		try {
			fos = new FileOutputStream(FILE_NAME);
			char rand;

			for (int i = 0; i < 15; i++) {

				for (int j = 0; j < 15; j++) {
					rand = (char)(Math.random() * 26 + 'A');//char value of 'A' is 65 so +65 could also be used.
					// the code above selects a random letter (uppercase)
					// that is anywhere from A to Z.
					fos.write((byte) rand);// has to be cast because fileoutstrea does it a byte at a time
					if (j < 14) {
						fos.write(',');
					}
				} // j loop
				fos.write('\n');
			} // i loop
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (fos != null) {
				fos.close();
			}
		}
	}

	public void fisExample() throws IOException {
		try {
			fis = new FileInputStream(FILE_NAME);
			byte in;

			// when reading in bytes, if you ever get -1, this means you've reached the end
			// of the file
			while ((in = (byte) fis.read()) != -1) {
				System.out.print((char) in);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			if(fis != null) {
				fis.close();
			}
		}
	}
	public void fileWriterAndReaderExample() throws IOException{
		try {
			fr = new FileReader(FILE_NAME);
			fw = new FileWriter(FILE_NAME.split("\\.")[0] + "_lowerCase." +
			FILE_NAME.split("\\.")[1]);//this writes to a file called: myfile_lowercase.csv
			int in;//input
			while((in = fr.read()) != -1) {
				if(in == '\n' || in == ',') {
					fw.write((char)in);
				}else
				fw.write((char)(in + 32));
			}
		}catch(IOException e) {
			e.printStackTrace();
		}finally {
			if(fr != null)
				fr.close();
			if(fw != null)
				fw.close();
		}
	}
	public void bufferedExample() throws IOException{
		try {
			br = new BufferedReader(new FileReader(FILE_NAME));
			bw = new BufferedWriter(new FileWriter(FILE_NAME.split("\\.")[0] + "_replaced." +
				FILE_NAME.split("\\.")[1]));
		
		String input = "";
		while((input = br.readLine()) != null) {
			input = input.replaceAll("A", "REPLACED");
			input += '\n';
			bw.write(input);
		}
	}catch (IOException e) {
		e.fillInStackTrace();
	}finally {
		if(br != null)
			br.close();
		if(bw != null)
			bw.close();
	}
}
}
