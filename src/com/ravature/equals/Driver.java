package com.ravature.equals;

import com.revature.comparing.Person;

public class Driver {

	public static void main(String[] args) {
		Person p1 = new Person("Mary Sue", 18);
		Person p2 = new Person("Marty Stue", 1556);
		Person p3 = new Person("Marylin Stu", 80);
		Person p4 = new Person("Gertrude", 10000);
		Person p5 = new Person("Zelda", 50);
		Person p6 = new Person("Zelda", 50);
		
		System.out.println(p1 == p2);//false (duh!)
		System.out.println(p1.equals(p2));//false (duh!) values of object are different
		System.out.println(p5 == p6); //points to different points in memory because differnt objects
		System.out.println(p5.equals(p6));//true using equals method created in person.java
	}

}
